import argparse
import datetime
import os
import random
import time

from parser import generar_mensaje
from emailer import enviar_mail
from scrapper import conseguir_datos

def main():
    #Parseo de argumentos
    parser = argparse.ArgumentParser()
    parser.add_argument("-debug", type=bool, nargs="?", default=False)
    parser.add_argument("-silenciado", type=bool, nargs="?", default=False)
    args = parser.parse_args()
    #Cuerpo del programa
    APERTURA_RUEDA = datetime.time(9,45) #algo de tolerancia
    CIERRE_RUEDA = datetime.time(18,15) #algo de tolerancia
    TIEMPO_ACTUALIZACION = 300+random.randint(60,180)
    # HOY = datetime.date.today().isoformat()
    # destinatarios = {'vladcou@gmail.com':["C", "KO", "MMM", "PFE", "T", "XOM"]}
    destinatarios = ['vladcou@gmail.com']
    hora = datetime.datetime.now().time()
    debug = args.debug
    if debug:
        debug = True
        APERTURA_RUEDA = datetime.time(hora.hour, hora.minute-1)
        CIERRE_RUEDA = datetime.time(hora.hour, hora.minute+1)
    while APERTURA_RUEDA<=hora and hora<=CIERRE_RUEDA:
        try:
            conseguir_datos()
        except:
            print("ALGO PASO")
            pass    
        if not args.silenciado:
            print(f"Datos actualizados {datetime.datetime.now().time().strftime('%H:%M:%S')}, bosmang!")
        mensaje = generar_mensaje()
        if mensaje:
            enviar_mail(mensaje, destinatarios, debug)
            if not args.silenciado:
                print(f"Mensaje enviado a las {hora.strftime('%H:%M:%S')}, bosmang!")
            time.sleep(TIEMPO_ACTUALIZACION)
            hora = datetime.datetime.now().time()
        else:
            if not args.silenciado:
                print(f"Sin data nueva a las {hora.strftime('%H:%M:%S')}, bosmang!")
            time.sleep(TIEMPO_ACTUALIZACION)
            hora = datetime.datetime.now().time()
    try:
        os.remove('mensaje.tmp')
        os.remove('chromedriver.log')
    except FileNotFoundError:
        pass
    if not args.silenciado:
        print("Trabajo terminado, bosmang!")

if __name__ == '__main__':
    main()
