import datetime
import re
def generar_mensaje():
    """
    Devolver el mensaje para enviar luego de procesar los datos.
    """
    dia_iso = datetime.date.today().isoformat()

    REGEX_NUEVAS_ON = re.compile(r"(\d+:\d+,.*Aviso de suscripción de Obligaciones Negociables.*)")
    archivo = f"{dia_iso}.csv"
    with open(archivo, "r") as f:
        nuevas_ons_matchs = [re.search(REGEX_NUEVAS_ON, lines) for lines in f.readlines()]
        lista_nuevas_on = [i.group(0) for i in nuevas_ons_matchs if i]
    contenido = "\n".join(lista_nuevas_on)
    dia = datetime.date.today().strftime("%d/%m/%Y")
    mensaje = f"Oi pampa! BolsarEmailer reportando que en el dia {dia} le tiraron data de:\n{contenido}"
    if len(lista_nuevas_on) == 0:
        return 0
    else:
        try: 
            with open('mensaje.tmp', 'r') as temporal:
                if temporal.read() == mensaje:
                    return 0
        except FileNotFoundError:
            with open('mensaje.tmp', 'x') as temporal:
                pass     
        with open('mensaje.tmp', 'w') as temporal:
            print(mensaje, file=temporal, end='', sep='')
            mensaje_formateado = re.sub('\n', '\r\n', mensaje)
            return mensaje_formateado