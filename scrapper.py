from bs4 import BeautifulSoup
import datetime
import pandas as pd
import random
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import time

class JavaScriptError(Exception):
    def __init__(self, message):
        super().__init__(message)

def revisar_error_js():
    """
    Revisar el navegador por errores severos, devolverlos junto con un mensaje.
    Parameters:
        driver (selenium.webdriver.firefox.webdriver.WebDriver): Driver que se está utilizando.
    Returns:
        errors (list, str): Lista con los errores detectados.
        message (str): Error de mensaje.
    """
    with open("chromedriver.log", "r") as log:
        error = re.match("JavaScript error", log.read())
    if error:
        mensaje = "JavaScript error encontrado!"
        raise JavaScriptError(mensaje)

def conseguir_datos():
    """
    Conseguir datos diarios de bolsar.
    Genera un archivo .csv con la fecha del dia en formato ISO.
    """
    hoy_iso = datetime.date.today().isoformat()
    hoy = datetime.date.today().strftime("%d/%m/%Y")
    # ayer = datetime.date(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day-1).strftime("%d/%m/%Y")
    # fecha_ayer = datetime.date(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day-1).isoformat()
    fecha = hoy
    BOLSAR_URL = "https://www.bolsar.com/Vistas/Noticias/InformacionesRelevantes.aspx"
    TIEMPO_ESPERA = 60+random.randint(5,10) #randomizar un poco cuanto espera para enmascarar el request
    options = webdriver.ChromeOptions()
    options.headless = True
    driver = webdriver.Chrome(options=options, service_args=["--verbose", "--log-path=./chromedriver.log"])
    driver.set_page_load_timeout(60)
    driver.get(BOLSAR_URL)
    element_desde = driver.find_element_by_id("ctl00_ctl00_ContentPlaceHolder1_tablaContenidoFiltro_txbFechaDesde")
    element_hasta = driver.find_element_by_id("ctl00_ctl00_ContentPlaceHolder1_tablaContenidoFiltro_txbFechaHasta")
    element_desde.clear()
    element_hasta.clear()
    element_desde.send_keys(fecha)
    element_desde.send_keys(Keys.ENTER)
    element_hasta.send_keys(fecha)
    element_hasta.send_keys(Keys.ENTER)
    element_paginas = driver.find_elements_by_id("ctl00_ctl00_ContentPlaceHolder1_GrillaListado_dataGridListado_ctl14_cboPages")
    select = Select(element_paginas[0])
    select.select_by_visible_text("100")
    estado_inicial = driver.find_element_by_id("__VIEWSTATE") # __EVENTVALIDATION, __EVENTTARGET tambien funcionan.
    estado_final = driver.find_element_by_id("__VIEWSTATE") # __EVENTVALIDATION, __EVENTTARGET tambien funcionan.
    while estado_inicial == estado_final:
        try:
            revisar_error_js()
        except JavaScriptError:
            break
        time.sleep(TIEMPO_ESPERA)
        estado_final = driver.find_element_by_id("__VIEWSTATE") # __EVENTVALIDATION, __EVENTTARGET tambien funcionan.
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    datos = soup.find(id='ctl00_ctl00_ContentPlaceHolder1_GrillaListado_dataGridListado')
    dfs = pd.read_html(str(datos), header=0)
    datos_df = dfs[0].loc[: ,'Fecha':'Referencia'].drop('Especies', axis=1)[:-2]
    with open(f"{hoy_iso}.csv", 'w') as file:
        datos_df.to_csv(file, header=file.tell()==0, index_label=False, index=False)
    driver.close()

