import datetime
from email.message import EmailMessage
import os
import re
import smtplib

def enviar_mail(mensaje, destinatarios, debug=False):
    """
    Enviar mails con el tipo de noticias seleccionado.

    Parameters:
        destinatarios (list, str): Lista de mails a los que se enviará la alerta.
        tipo_noticia (list, str): Tipo de activo sobre el que se quiere recivir alertas.
        debug (bool): Mandar mail posta o mail a la terminal de debugging. Default: False
    """
    EMAIL_ADDRESS = os.environ.get('EMAILER_DIR')
    EMAIL_PASSWORD = os.environ.get('EMAILER_PASS')
    msg = EmailMessage()
    msg['Subject'] = "Nueva suscripción a ON!"
    msg['From'] = EMAIL_ADDRESS
    msg['Bcc'] = destinatarios
    msg.set_content(str(mensaje))
    if debug: 
        with smtplib.SMTP('localhost', 1025) as smtp: #Correr en terminal: python -m smtpd -c DebuggingServer -n localhost:1025 y ahi llegaran los mails 
            smtp.send_message(msg)
    else:
        with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
            smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
            smtp.send_message(msg)